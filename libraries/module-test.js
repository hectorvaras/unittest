function sayMyname(name) {
    return `Your name is ${name}`;
}

function myFunction(condition, callback){
    if(condition){
        callback();
    }
}

module.exports = {sayMyname, myFunction};