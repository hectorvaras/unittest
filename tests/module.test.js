var chai = require('chai');
var sinon = require('sinon');
var sayMyname = require('../libraries/module-test').sayMyname;
var myFunction = require('../libraries/module-test').myFunction;
var expect = chai.expect;
var assert = chai.assert;


describe('## MODULE TEST', () => {
    describe('# Say my name ', () => {
        it('should show my name ', () => {
            expect(sayMyname('Hector')).to.be.equal('Your name is Hector');
        });
        it('should sayMyname to be a string ', () => {
            expect(sayMyname('Hector')).to.be.a('string');
        });
    });
    describe('# myFunction', () => {
        it('should call the callback function', () => {
            var callback = sinon.spy();

            myFunction(true, callback);

            assert(callback.calledOnce, 'Condition was false');
        });
    });
});

